<?php 
  /**
   * SCALAR TYPES
   *  
   * bool TRUE or FALSE
   * int  5, 4, 6
   * float 5.5, 4.5, 6.5
   * string  "Hello World"
   * 
   * CONPOUND YPES
   * array [1,2,3,4,5]
   * object  ["NAME" => "Juan", "AGE" => "30"]
   * callable 
   * iterable
   * 
   * 
   * 
   * SPECIAL TYPES
   * resource
   * NULL
   * 
  */


  /**
   * gettype() retorna el tipo de dato de una variable
   * si usas var_dump() se puede ver el tipo de dato de la variable
   * 
  */

  /**
   * si declaramos una variable con un tipo string  $var = '4'; 
   * podemos convertirla en un int solo agregando esto $var = (int) '4'; 
   * 
   * ahora la variable pasa de ser un string a un int
  */

?>