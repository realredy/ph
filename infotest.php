<?php
 /**
  * muchas veces queremos colocar mucho testo y con el mismo colocar 
  * variables dentro del testo y para lo el caso debemos usar contatenaciones 
  * entonces esta expresion puede ser vastatnte util
 */
   $data = 'una nueva variable';
    $numero = 55454545;
  $mitestoCompuesto = <<<TEXT
    Dentro de este parrafo podemos concatenar $data 
    y por otro lado podemos colocar multiple saltos de linea  
    y en donde incluso podemos colocar un numero como este:  $numero
   TEXT;

   echo "$mitestoCompuesto\n";
   echo "<hr>";
   // si querremos respetat el salto de linea 

   echo nl2br($mitestoCompuesto);


   /**
    * UNA IMPORTANTE MANERA DE DESTUIR UNA VARIABLE O PASARLA A NULL ES: 
    * unset($variable);
   */

?>