<?php

echo str_replace ('\\','/', __DIR__).'/Clasess/CollectionClass/RealClass.php'.'<hr>';
 
require_once './Clasess/CollectionClass/RealClass.php';

 use Clasess\CollectionClass\RealClass;
 
if(class_exists('Clasess\CollectionClass\RealClass')): 
echo  'la clase existe '.'<hr>';
endif;

$showclass = new RealClass();

echo "<pre>";
var_dump($showclass);
echo "</pre>";

// cuando declaras una constante, clase o funcion sin un namespace esta podra ser 
// colocada en espacio global, esto significa que al llamar a este archivo 
// si la clase es llamada Myclase la misma tendra un espacio global y esto signi-
// fica que no puedes tener otra clase o funcion o constante con este nombre
// y es para lo que se usan los nombres de espacio para poder usar classes y 
// demas con el mismo nombre sin causar conflicto.


// OUTPUT:
 
/*  C:/xampp/htdocs/ph/Clasess/CollectionClass/RealClass.phpla 
____________________________________________________________________
  clase existe RealClass:
____________________________________________________________________
  object(Clasess\CollectionClass\RealClass)#1 (0) {
  }
*/