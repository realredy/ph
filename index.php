
<?php 
#esto es solo un comentario, o una una nueva manera
echo "hola mundo \n \r \$ <br>";
echo "otra linea "."<hr>";


$subsecy[] = 'Cancino';
$subsecy[] = 'piedra blanca';
$subsecy[] = 'bahoruco';
#--------------------------------
$object[] = 'honduras';
$object[] = 'mexico';
$object[] = 'peru';
$object[] = $subsecy;



#---------------------------------------
$otroObj['CUDADES'][] = 'NEW YORK';
$otroObj['CUDADES'][] = 'ATLANTA';
$otroObj['CUDADES'][] = 'GEORGEA';
#var_dump($object);

foreach($object as $mes){
    
     if(!is_array($mes) ){
          echo $mes.'<br>';
     } 

    if(is_array($mes) ){
        foreach($mes as $mesa){
            echo $mesa.'<br>';
        }
    }
    
}
// echo $codif;
  var_dump($otroObj);

 echo "<hr>";

 $codif = json_encode($otroObj);
 ?>
 <script>
 // enviando una nueva forma de objeto
console.log(<?php echo $codif; ?>);
/*
# CIUDADES:
# 0: "NEW YORK"
# 1: "GEORGEA"
# 2: "ATLANTA"
*/
</script>

<?php 
/*
# Ejercicios con operadores logicos de comparacion 
# en la nueva version de php 7...
*/
// operador condicional lineal
$var = "";// variable declarada que puede dar null o cadena vacia
echo "<hr>";
# Este resultado sera = cadena vacia, su nombre es OPERADOR TERNARIO
echo 'Mi cadena = '. (($var == '') ? 'cadena vacia!' : $var);

# OPERADOR DE UNION NULL 
# Este operador devolvera el primer valor que no sea null
$valor1; # Una variable no inicializada es igual a null si es = ''; no es null, solo esta vacia;
$valor2 = 'Latop';
$valor3 = null;
# La sentencia sera igual a  Latop ya que este condicional devuelve el primer elemento no null
echo '<hr>';
echo 'Devuelve el valor de: ' . ($valor1?? $valor2?? $valor3?? 'valor deconocido!'); 

/*
| como ciudades es una tabla anidada ver linea 48 
| array(1) { ["CUDADES"]=> array(3) { [0]=> string(8) "NEW YORK" [1]=> string(7) "ATLANTA" [2]=> string(7) "GEORGEA" } }
| 
*/

$ancho_array = count($otroObj['CUDADES']); # guardamos el valor total de la cantidad de items en la cadena 
echo '<hr>';
$counter = 0; # iniciamos el contador en cero
while($counter < $ancho_array){ # condicionamos el contador para iniciar el bucle
   echo $otroObj['CUDADES'][$counter].'<br>'; # imprimimos con un salto de linea
   $counter++; # incrementamos el contador por cada bucle
}


  //----------------------------------------------
# | PRACTICA CON WHILE CON EL CONDICIONAL TERNARIO
  //----------------------------------------------


?>
<hr style="border: dashed 1px red;">
<ol>
<?php 
$ancho_array = count($otroObj['CUDADES']); # guardamos el valor total de la cantidad de items en la cadena 
$counter = 0; 
# al usar esta sintaxis debemis usar endwhile :::: while(): endwhile;
while ($counter < $ancho_array) :
    echo '<li>'. $otroObj['CUDADES'][$counter].'</li>'; 
    $counter++;
endwhile;
?>
</ol>


<span>--------------INTERRUPCION DEL BUCLE-------------</span><br>
<?php 

# El uso de continue evalua una sentencia dentro del blucle y se ser verdadera
# omite estas clausulas y continua el bucle 
/*
*  si utilizaramos BREAK el siclo sería interrumpido y no elecuraría las demas iteracciones
 */
foreach($object as $mes){
  if(!is_array($mes) ){
       echo $mes.'<br>';
  } 
 if(is_array($mes) ){
    continue;
    // break; iterrumpiria todo el bucle
 }
}